package selenium.junit;


import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.AfterTestExecutionCallback;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.TestWatcher;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.io.FileHandler;
import org.openqa.selenium.support.ui.WebDriverWait;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;


public class ChromeTest {

    WebDriver driver;
    WebDriverWait wait;

    @BeforeEach
    public void setUp(){
        try{
            driver = new ChromeDriver();
            driver.get("https://squash-tf.readthedocs.io/en/latest/");
            
        }
        catch(Exception e){
            System.err.println(e.getMessage());
        }
        driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
        wait = new WebDriverWait(driver,30);
    }

    @Test
    public void chromeTest() throws InterruptedException {
        driver.findElement(By.xpath("//a[text()='Execution Server']")).click();
        String value = driver.findElement(By.xpath("//div[@id='squash-tf-execution-server']")).getAttribute("class");
        Assertions.assertTrue(value.equals("section"), "La classe n'est pas correcte");
    }

    @AfterEach
    public void tearDown(TestInfo testInfo) throws IOException {
        TakesScreenshot scrShot =((TakesScreenshot)driver);
        File SrcFile=scrShot.getScreenshotAs(OutputType.FILE);
        File DestFile=new File("target/" + testInfo.getDisplayName().replace("()","") + ".png");
        FileUtils.copyFile(SrcFile, DestFile);
        driver.quit();
    }


}
