Ce projet exécute des tests Selenium sur des pages disponibles sur la documentatiön Squash TF puis effectue des captures avant de 
fermer le navigateur.

Les captures d'écran des tests de la classe FirefoxTest sont enregistrées dans target/subFolder.

Celles de la classe ChromeTest sont directement dans le target.

Ces captures ont un nom basé sur le format suivant : nom_de_la_methode.png